#ifndef TABLE_GRAMMAR_H
#define TABLE_GRAMMAR_H

#include <iostream>
#include <unordered_map>
#include <list>

using namespace std;

namespace grammar_table {

unordered_map< string, unordered_map< string, list<string> > >  dict
{
    {
        { "S",
            {
                {"<DATA_TYPE>", list<string>({"BLOCK", "MAIN"}) },
                {"<ID>", list<string>({"BLOCK", "MAIN"}) },
                {"<IF>", list<string>({"BLOCK", "MAIN"}) },
                {"<WHILE>", list<string>({"BLOCK", "MAIN"}) },
                {"<BREAK>", list<string>({"BLOCK", "MAIN"}) },
                {"<RETURN>", list<string>({"BLOCK", "MAIN"}) },
                {"<PRINT>", list<string>({"BLOCK", "MAIN"}) },
                {"<DEF>", list<string>({"BLOCK", "MAIN"}) },
                {"<IMPORT>", list<string>({"BLOCK", "MAIN"}) },
                {"<MAIN>", list<string>({"MAIN"}) },
                {"$", list<string>({"€",}) },
            }
        },
        { "BLOCK",
            {
                {"<DATA_TYPE>", list<string>({"DECLARATION", "BLOCK"}) },
                {"<ID>", list<string>({"DECLARATION", "BLOCK"}) },
                {"<IF>", list<string>({"DECLARATION", "BLOCK"}) },
                {"<WHILE>", list<string>({"DECLARATION", "BLOCK"}) },
                {"<BREAK>", list<string>({"DECLARATION", "BLOCK"}) },
                {"<RETURN>", list<string>({"DECLARATION", "BLOCK"}) },
                {"<PRINT>", list<string>({"DECLARATION", "BLOCK"}) },
                {"<DEF>", list<string>({"FUNCTION", "BLOCK"}) },
                {"<IMPORT>", list<string>({"IMPORT", "BLOCK"}) },
                {"<MAIN>", list<string>({"€",}) },
            }
        },
        { "MAIN",
            {
                {"<MAIN>", list<string>({"<MAIN>", "<OPEN_PARENTHESIS>", "PARAMS_FUNCTION", "<CLOSE_PARENTHESIS>", "<OPEN_BRACE>", "CONTEXT", "<CLOSE_BRACE>"}) },
                {"$", list<string>({"€",}) },
            }
        },
        { "DECLARATION",
            {
                {"<DATA_TYPE>", list<string>({"ASIGNATION", "<SEMICOLON>"}) },
                {"<ID>", list<string>({"ASIGNATION", "<SEMICOLON>"}) },
                {"<IF>", list<string>({"CONDITIONAL",}) },
                {"<WHILE>", list<string>({"BUCLE",}) },
                {"<BREAK>", list<string>({"COMMAND","<SEMICOLON>"}) },
                {"<RETURN>", list<string>({"COMMAND","<SEMICOLON>"}) },
                {"<PRINT>", list<string>({"COMMAND","<SEMICOLON>"}) },
            }
        },
        { "CONTEXT",
            {
                {"<DATA_TYPE>", list<string>({"DECLARATION", "CONTEXT"}) },
                {"<ID>", list<string>({"DECLARATION", "CONTEXT"}) },
                {"<IF>", list<string>({"DECLARATION", "CONTEXT"}) },
                {"<WHILE>", list<string>({"DECLARATION", "CONTEXT"}) },
                {"<BREAK>", list<string>({"DECLARATION", "CONTEXT"}) },
                {"<RETURN>", list<string>({"DECLARATION", "CONTEXT"}) },
                {"<PRINT>", list<string>({"DECLARATION", "CONTEXT"}) },
                {"<CLOSE_BRACE>", list<string>({"€",}) },
            }
        },
        { "ASIGNATION",
            {
                {"<DATA_TYPE>", list<string>({"<DATA_TYPE>", "<ID>", "FACT_ASIGNATION"}) },
                {"<ID>", list<string>({"<ID>", "EXIST_ASIGNATION_CALL"}) },
            }
        },
        { "FACT_ASIGNATION",
            {
                {"<EQUAL>", list<string>({"NEW_ASIGNATION"}) },
                {"<OPEN_BOX_BRACKET>", list<string>({"NEW_ASIGNATION"}) },
                {"<SEMICOLON>", list<string>({"€",}) },
            }
        },
        { "NEW_ASIGNATION",
            {
                {"<EQUAL>", list<string>({"<EQUAL>", "VALUE"}) },
                {"<OPEN_BOX_BRACKET>", list<string>({"<OPEN_BOX_BRACKET>", "<INT>", "<CLOSE_BOX_BRACKET>", "FACT_ACCES_ARRAY"}) },
            }
        },
        { "FACT_ACCES_ARRAY",
            {
                {"<EQUAL>", list<string>({"<EQUAL>", "ARRAY"}) },
                {"<SEMICOLON>", list<string>({"€",}) },
            }
        },
        { "EXIST_ASIGNATION_CALL",
            {
                {"<EQUAL>", list<string>({"<EQUAL>", "VALUE_ASIGNED"}) },
                {"<OPEN_PARENTHESIS>", list<string>({"CALL_FUNCTION",}) },
                {"<OPEN_BOX_BRACKET>", list<string>({"<OPEN_BOX_BRACKET>", "<INT>", "<CLOSE_BOX_BRACKET>", "EXIST_ASIGNATION_CALL",}) },
            }
        },
        { "VALUE_ASIGNED",
            {
                {"<ID>", list<string>({"VALUE"}) },
                {"<INT>", list<string>({"VALUE"}) },
                {"<FLOAT>", list<string>({"VALUE"}) },
                {"<BOOLEAN>", list<string>({"VALUE"}) },
                {"<STRING>", list<string>({"VALUE"}) },
                {"<OPEN_PARENTHESIS>", list<string>({"VALUE"}) },
                {"<OPEN_BOX_BRACKET>", list<string>({"ARRAY",}) },
            }
        },
        { "VALUE",
            {
                {"<ID>", list<string>({"OPERATION_VALUES"}) },
                {"<INT>", list<string>({"OPERATION_VALUES"}) },
                {"<FLOAT>", list<string>({"OPERATION_VALUES"}) },
                {"<BOOLEAN>", list<string>({"OPERATION_VALUES"}) },
                {"<STRING>", list<string>({"OPERATION_VALUES"}) },
                {"<OPEN_PARENTHESIS>", list<string>({"<OPEN_PARENTHESIS>", "VALUE", "<CLOSE_PARENTHESIS>"}) },
            }
        },
        { "OPERATION_VALUES",
            {
                {"<ID>", list<string>({"<ID>", "OPERATION_VALUES_AUX"}) },
                {"<INT>", list<string>({"CONSTANT", "OPERATION_VALUES_AUX"}) },
                {"<FLOAT>", list<string>({"CONSTANT", "OPERATION_VALUES_AUX"}) },
                {"<BOOLEAN>", list<string>({"CONSTANT", "OPERATION_VALUES_AUX"}) },
                {"<STRING>", list<string>({"CONSTANT", "OPERATION_VALUES_AUX"}) },
            }
        },
        { "OPERATION_VALUES_AUX",
            {
                {"<OPEN_PARENTHESIS>", list<string>({"CALL_FUNCTION", "OPERATION_VALUES_AUX"}) },
                {"<OPEN_BOX_BRACKET>", list<string>({"ACCES_ARRAY", "OPERATION_VALUES_AUX"}) },
                {"<ARITHMETIC_OPERATOR>", list<string>({"<ARITHMETIC_OPERATOR>", "OPERATION_VALUES_2", "OPERATION_VALUES_AUX",}) },
                {"<BINARY_OPERATOR>", list<string>({"<BINARY_OPERATOR>", "OPERATION_VALUES_2", "OPERATION_VALUES_AUX"}) },
                {"<SEMICOLON>", list<string>({"€",}) },
                {"<CLOSE_PARENTHESIS>", list<string>({"€",}) },
                {"<COMMA>", list<string>({"€",}) },
            }
        },

        { "OPERATION_VALUES_2",
            {
                {"<ID>", list<string>({"<ID>",}) },
                {"<INT>", list<string>({"CONSTANT",}) },
                {"<FLOAT>", list<string>({"CONSTANT",}) },
                {"<BOOLEAN>", list<string>({"CONSTANT",}) },
                {"<STRING>", list<string>({"CONSTANT",}) },
            }
        },

        {"CONSTANT",
            {
                {"<INT>", list<string>({"<INT>",}) },
                {"<FLOAT>", list<string>({"<FLOAT>",}) },
                {"<BOOLEAN>", list<string>({"<BOOLEAN>",}) },
                {"<STRING>", list<string>({"<STRING>",}) },
            }
        },
        {"CALL_FUNCTION",
            {
                {"<OPEN_PARENTHESIS>", list<string>({"<OPEN_PARENTHESIS>", "PARAMS", "<CLOSE_PARENTHESIS>",}) },
            }
        },
        {"ACCES_ARRAY",
            {
                {"<OPEN_BOX_BRACKET>", list<string>({"<OPEN_BOX_BRACKET>", "<INT>", "<CLOSE_BOX_BRACKET>",}) },
            }
        },
        {"IF_CONDITIONAL",
            {
                {"<IF>", list<string>({"<IF>", "<OPEN_PARENTHESIS>", "VALUE", "<CLOSE_PARENTHESIS>", "<OPEN_BRACE>", "CONTEXT", "<CLOSE_BRACE>",}) },
            }
        },
        {"ELSE_IF_CONDITIONAL",
            {
                {"<ELSE>", list<string>({"<ELSE>", "ELSE_IF_AUX",}) },
                {"<PRINT>", list<string>({"€",}) },
                {"<RETURN>", list<string>({"€",}) },
                {"<BREAK>", list<string>({"€",}) },
                {"<WHILE>", list<string>({"€",}) },
                {"<IF>", list<string>({"€",}) },
                {"<ID>", list<string>({"€",}) },
                {"<DATA_TYPE>", list<string>({"€",}) },
                {"<MAIN>", list<string>({"€",}) },
                {"<IMPORT>", list<string>({"€",}) },
                {"<DEF>", list<string>({"€",}) },
                //ESSTA CHANCADO
                {"<CLOSE_BRACE>", list<string>({"€",}) },
            }
        },
        {"ELSE_IF_AUX",
            {
                {"<IF>", list<string>({"<IF>", "<OPEN_PARENTHESIS>", "VALUE", "<CLOSE_PARENTHESIS>", "<OPEN_BRACE>", "CONTEXT", "<CLOSE_BRACE>", "<ELSE>", "ELSE_IF_AUX",}) },
                {"<OPEN_BRACE>", list<string>({"<OPEN_BRACE>", "CONTEXT", "<CLOSE_BRACE>",}) },
            }
        },
        {"CONDITIONAL",
            {
                {"<IF>", list<string>({"IF_CONDITIONAL", "ELSE_IF_CONDITIONAL",}) },
            }
        },
        {"BUCLE",
            {
                {"<WHILE>", list<string>({"<WHILE>", "<OPEN_PARENTHESIS>", "VALUE", "<CLOSE_PARENTHESIS>", "<OPEN_BRACE>", "CONTEXT", "<CLOSE_BRACE>",}) },
            }
        },
        {"COMMAND",
            {
                {"<BREAK>", list<string>({"<BREAK>",}) },
                {"<RETURN>", list<string>({"<RETURN>", "RETURN_AUX",}) },
                {"<PRINT>", list<string>({"PRINT",}) },
            }
        },
        { "RETURN_AUX",
            {
                {"<ID>", list<string>({"VALUE",}) },
                {"<INT>", list<string>({"VALUE",}) },
                {"<FLOAT>", list<string>({"VALUE",}) },
                {"<BOOLEAN>", list<string>({"VALUE",}) },
                {"<STRING>", list<string>({"VALUE",}) },
                {"<OPEN_PARENTHESIS>", list<string>({"VALUE",}) },
                {"<SEMICOLON>", list<string>({"€",}) },
            }
        },
        {"PRINT",
            {
                {"<PRINT>", list<string>({"<PRINT>", "<OPEN_PARENTHESIS>", "VALUE", "<CLOSE_PARENTHESIS>",}) },
            }
        },
        {"IMPORT",
            {
                {"<IMPORT>", list<string>({"<IMPORT>", "<ID>",}) },
            }
        },
        { "PARAMS",
            {
                {"<ID>", list<string>({"PARAMS_AUX",}) },
                {"<INT>", list<string>({"PARAMS_AUX",}) },
                {"<FLOAT>", list<string>({"PARAMS_AUX",}) },
                {"<BOOLEAN>", list<string>({"PARAMS_AUX",}) },
                {"<STRING>", list<string>({"PARAMS_AUX",}) },
                {"<OPEN_PARENTHESIS>", list<string>({"PARAMS_AUX",}) },
                {"<CLOSE_PARENTHESIS>", list<string>({"€",}) },
            }
        },
        { "PARAMS_AUX",
            {
                {"<ID>", list<string>({"VALUE", "PARAMS_AUX_2",}) },
                {"<INT>", list<string>({"VALUE", "PARAMS_AUX_2",}) },
                {"<FLOAT>", list<string>({"VALUE", "PARAMS_AUX_2",}) },
                {"<BOOLEAN>", list<string>({"VALUE", "PARAMS_AUX_2",}) },
                {"<STRING>", list<string>({"VALUE", "PARAMS_AUX_2",}) },
                {"<OPEN_PARENTHESIS>", list<string>({"VALUE", "PARAMS_AUX_2",}) },
            }
        },
        { "PARAMS_AUX_2",
            {
                {"<COMMA>", list<string>({"<COMMA>", "VALUE", "PARAMS_AUX_2",}) },
                {"<CLOSE_PARENTHESIS>", list<string>({"€",}) },
            }
        },

        { "FUNCTION",
            {
                {"<DEF>", list<string>({"<DEF>", "<DATA_TYPE>", "<ID>", "<OPEN_PARENTHESIS>", "PARAMS_FUNCTION", "<CLOSE_PARENTHESIS>", "<OPEN_BRACE>", "CONTEXT", "<CLOSE_BRACE>",}) },
            }
        },

        { "PARAMS_FUNCTION",
            {
                {"<DATA_TYPE>", list<string>({"PARAMS_FUNCTION_AUX",}) },
                {"<CLOSE_PARENTHESIS>", list<string>({"€",}) },
            }
        },

        { "PARAMS_FUNCTION_AUX",
            {
                {"<DATA_TYPE>", list<string>({"<DATA_TYPE>", "<ID>", "PARAMS_FUNCTION_2",}) },
            }
        },

        { "PARAMS_FUNCTION_2",
            {
                {"<COMMA>", list<string>({"<COMMA>", "<DATA_TYPE>", "<ID>", "PARAMS_FUNCTION_2",}) },
                {"<CLOSE_PARENTHESIS>", list<string>({"€",}) },
            }
        },

        { "PARAMS_ARRAY",
            {
                {"<INT>", list<string>({"PARAMS_ARRAY_AUX",}) },
                {"<FLOAT>", list<string>({"PARAMS_ARRAY_AUX",}) },
                {"<BOOLEAN>", list<string>({"PARAMS_ARRAY_AUX",}) },
                {"<STRING>", list<string>({"PARAMS_ARRAY_AUX",}) },
                {"<CLOSE_BOX_BRACKET>", list<string>({"€",}) },
            }
        },

        { "PARAMS_ARRAY_AUX",
            {
                {"<INT>", list<string>({"CONSTANT", "PARAMS_ARRAY_2",}) },
                {"<FLOAT>", list<string>({"CONSTANT", "PARAMS_ARRAY_2",}) },
                {"<BOOLEAN>", list<string>({"CONSTANT", "PARAMS_ARRAY_2",}) },
                {"<STRING>", list<string>({"CONSTANT", "PARAMS_ARRAY_2",}) },
            }
        },
        { "PARAMS_ARRAY_2",
            {
                {"<COMMA>", list<string>({"<COMMA>", "CONSTANT", "PARAMS_ARRAY_2",}) },
                {"<CLOSE_BOX_BRACKET>", list<string>({"€",}) },
            }
        },
        { "ARRAY",
            {
                {"<OPEN_BOX_BRACKET>", list<string>({"<OPEN_BOX_BRACKET>", "PARAMS_ARRAY", "<CLOSE_BOX_BRACKET>",}) },
            }
        },



        //{ "BLOCK", {{"<DATA_TYPE>", ""}} },

//        { "b", {"d", 4} }
    }
};

list<string> get_item_table(string production, string terminal){
    return dict[production][terminal];
}

}



#endif // TABLE_GRAMMAR_H
