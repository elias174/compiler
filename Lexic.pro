TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt
CONFIG += c++11
LIBS += \
       -lboost_system\

SOURCES += main.cpp

HEADERS += \
    lexic.h \
    syntaxic.h \
    table_grammar.h

DISTFILES += \
    file.cnn \
    ex_syntac.cnn
