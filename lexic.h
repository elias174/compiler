#ifndef LEXIC_H
#define LEXIC_H

#include <iterator>
#include <iostream>
#include <algorithm>
#include <string>
#include <regex>
#include <list>
#include <map>
#include <fstream>
#include <unordered_map>
#include <boost/algorithm/string.hpp>

using namespace std;

namespace lexic {

    struct LexicNode{
        string token;
        int n_line;
        string lex;

        LexicNode(string token, int n_line, string lex){
            this->token = token;
            this->n_line = n_line;
            this->lex = lex;
        }

        void print_lexic_node(){
            cout << this->n_line << "  " << this->token << "  " << this->lex << endl;
        }
    };

    vector<LexicNode> results_lexic;
    vector<LexicNode> errors;

    void print_results_lexic(vector<LexicNode> to_print){
        if(to_print.size()){
            for(auto it = to_print.begin(); it != to_print.end(); ++it){
                it->print_lexic_node();
            }
        }
    }

    bool exist_errors(){
        return errors.size() > 0;
    }

    bool check_token(string parsed, string &info){
        vector<pair<string,string>> patterns {
                    make_pair("^import", "<IMPORT>"),
                    make_pair("^break", "<BREAK>"),
                    make_pair("^while", "<WHILE>"),
                    make_pair("^return", "<RETURN>"),
                    make_pair("^print", "<PRINT>"),
                    make_pair("^main", "<MAIN>"),
                    make_pair("^def", "<DEF>"),
                    make_pair("^if", "<IF>"),
                    make_pair("^else", "<ELSE>"),
                    make_pair("^true$|^false$",  "<BOOLEAN>"),
                    make_pair("^[-]?([0-9]+)", "<INT>"),
                    make_pair("^[-]?([0-9]+[.]+)?[0-9]+", "<FLOAT>"),
                    make_pair("^int$|^float$|^bool$|^char$|^string$",  "<DATA_TYPE>"),
                    //make_pair("^[-]?(?=[.]?[0-9]+)[0-9]*(?:[.][0-9]*)?(?:[Ee][+-]?[0-9]+)?","NUMERIC_VALUE"),
                    //make_pair("^(?=.)([+-]?([0-9]*)(\.([0-9]+))?)$","NUMERIC_VALUE"),
                    make_pair("^[_a-zA-Z]+[_a-zA-Z0-9]*","<ID>"),
                    make_pair("^\\*|^\\+|^-|^/|^%",  "<ARITHMETIC_OPERATOR>"),
                    make_pair("^==$|^!=|^>=|^<=|^>|^<|^&&", "<BINARY_OPERATOR>"),
                    //make_pair("^*=$|^+=|^/=", "<ALTER_OPERATORS"),
                    make_pair("^\\,", "<COMMA>"),
                    make_pair("^\\;", "<SEMICOLON>"),
                    make_pair("^\\=$", "<EQUAL>"),
                    //make_pair("^\?//(.*)", "<COMMENTARY>"),
                    make_pair("^\\(", "<OPEN_PARENTHESIS>"),
                    make_pair("^\\)", "<CLOSE_PARENTHESIS>"),
                    make_pair("^\\[", "<OPEN_BOX_BRACKET>"),
                    make_pair("^\\]", "<CLOSE_BOX_BRACKET>"),
                    make_pair("^\\{", "<OPEN_BRACE>"),
                    make_pair("^\\}", "<CLOSE_BRACE>"),
        };

        auto pat = patterns.begin();
        while (pat != patterns.end() && parsed != ""){
            regex pattern = regex(pat->first);
            smatch m;
            if(regex_match(parsed, pattern)){
                info = pat->second;
                return true;
            }
            pat++;
        }
        return false;
    }


    bool exist_in_delimiters(string symbol){
        string DELIMITER = "= * , ; + % / == >= <= < > [ ] { } ( )";
        //string DELIMITER = "* , ; + % [ ] { } ( )";
        vector<string> delimiters;
        boost::algorithm::split(delimiters, DELIMITER, boost::is_any_of(" "), boost::token_compress_on);
        return std::find(delimiters.begin(), delimiters.end(), symbol) != delimiters.end();
    }

    vector<string> parse(string line, int n_line){
        vector<string> to_tokenize;
        if(line[0]=='"' && line[line.length()-1]=='"'){
            results_lexic.push_back(LexicNode("<STRING>", n_line, line.substr(1,line.length()-2)));
            // cout << n_line << "   " << "STRING" << "   " << line.substr(1,line.length()-2) << endl;
            return to_tokenize;
        }
        vector<string> splited;
        boost::algorithm::split(splited, line, boost::is_any_of("\t\n "), boost::token_compress_on);
        string info_token;
        for(auto it_split = splited.begin(); it_split != splited.end(); ++it_split){
            if(check_token((*it_split), info_token)){
                results_lexic.push_back(LexicNode(info_token, n_line, (*it_split)));
                //cout << n_line << "   " << info_token << "  "  << (*it_split) << endl;
            }
            else{
                string will_token = "";
                for(int i=0; i<(*it_split).length(); ++i){
                    string eval;

                    string info_token;
                    eval += (*it_split)[i];
                    if((i+1<(*it_split).length() && (*it_split)[i+1] == '=' && exist_in_delimiters(eval))){
                        eval += ((*it_split)[i+1]);
                        ++i;
                    }
                    string delimiter_token;
                    if( exist_in_delimiters(eval) && check_token(eval, delimiter_token)){
                        if(will_token != "" && check_token(will_token, info_token)){
                            results_lexic.push_back(LexicNode(info_token, n_line, will_token));
                            //cout << n_line << "   " << info_token << "  " << will_token << endl;
                        }
                        else if(will_token != "" && !check_token(will_token, info_token)){
                            errors.push_back(LexicNode("ERROR", n_line, will_token));
                            //cout << n_line << "   " << "ERROR" << "    " << will_token << endl;
                        }
                        results_lexic.push_back(LexicNode(delimiter_token, n_line, eval));
                        // cout << n_line << "   " << delimiter_token << "  "  << eval << endl;
                        will_token = "";
                    }
                    else if(eval == "-" && i>0 && !exist_in_delimiters(string(1,(*it_split)[i-1])) ){
                        if(will_token != "" && check_token(will_token, info_token)){
                            results_lexic.push_back(LexicNode(info_token, n_line, will_token));
                            // cout << n_line << "   " << info_token << "  " << will_token << endl;
                        }
                        else if(will_token != "" && !check_token(will_token, info_token)){
                            errors.push_back(LexicNode("ERROR", n_line, will_token));
                            // cout << n_line << "   " << "ERROR" << "    " << will_token << endl;
                        }
                        results_lexic.push_back(LexicNode("<ARITHMETIC_OPERATOR>", n_line, eval));
                        // cout << n_line << "   " <<  "ARITHMETIC_OPERATOR" << "  "  << eval << endl;
                        will_token = "";
                    }
                    else
                        will_token += (*it_split)[i];
                }
                string info_token;
                if(will_token != "" && check_token(will_token, info_token)){
                    results_lexic.push_back(LexicNode(info_token, n_line, will_token));
                    // cout << n_line << "   " << info_token << "  " << will_token << endl;
                    will_token = "";
                }
                else if(will_token != "" && !check_token(will_token, info_token)){
                    errors.push_back(LexicNode("ERROR", n_line, will_token));
                    // cout << n_line << "   " << "ERROR" << "    " << will_token << endl;
                    will_token = "";
                }
            }
        }
        return to_tokenize;
    }


    void pre_proc(string line, int n_line){
        vector<string> to_tokenize;

        int pos_i=0;
        int pos_j=0;
        bool finished = false;
        bool inside_quote = false;
        for(pos_i, pos_j; pos_i < line.length() && pos_j < line.length(); ++pos_j){
            if( line[pos_j] == '"' && pos_j > 0 && line[pos_i] != '"'){
                to_tokenize.push_back(line.substr(pos_i, pos_j-pos_i));
                inside_quote = true;
                pos_i = pos_j;
            }
            else if( line[pos_j] == '"' && pos_j > 0 && line[pos_i] == '"'){
                to_tokenize.push_back(line.substr(pos_i, pos_j - pos_i + 1));
                pos_i = pos_j+1;
                inside_quote = false;
            }
            else if( line[pos_j] == '/' && line[pos_j+1] == '/' && !inside_quote){
                to_tokenize.push_back(line.substr(pos_i, pos_j-pos_i));
                pos_i = pos_j;
                break;
            }
        }

        to_tokenize.push_back(line.substr(pos_i, line.length()));
        for(auto it = to_tokenize.begin(); it != to_tokenize.end(); ++it){
            if((*it)[0] == '/' && (*it)[1] == '/' ) continue;
            vector<string> results = parse((*it), n_line);
        }
    }


    bool es_NUM_FLOTANTE(string lexema){

        regex expr("[+-]?([0-9]+[.]+)?[0-9]+");

        return regex_match(lexema, expr);

    }

    void analyze_file(string file_name){
        ifstream file(file_name.c_str());
        string line;
        int i = 1;
        while(getline(file, line)){
            pre_proc(line, i);
            i++;
        }
    }
}



#endif // LEXIC_H
