#ifndef SYNTAXIC_H
#define SYNTAXIC_H

#include "lexic.h"
#include "table_grammar.h"
#include <iostream>
#include <stack>
#include <queue>

using namespace std;

namespace syntactic {
    struct NodeErrorSyntactic{
        int n_line;
        string error;
        NodeErrorSyntactic(int n_line, string error){
            this->n_line = n_line;
            this->error = error;
        }
    };

    vector<NodeErrorSyntactic> errors;

    bool is_terminal(string production){
        return production[0] == '<';
    }

    queue<lexic::LexicNode> process_lexic_results(vector<lexic::LexicNode> results){
        queue<lexic::LexicNode> ret;
        for(auto it = results.begin(); it != results.end(); ++it){
            ret.push((*it));
        }
        return ret;
    }

    // Temporal function
    void print_list(list<string> item){
        for(auto it = item.begin(); it != item.end(); ++it){
            cout << (*it) << endl;
        }
    }


    void sytax_analysis(string file){
        lexic::analyze_file(file);
        if(lexic::exist_errors()){
            cout << "Exist Lexic Errors" << endl;
            lexic::print_results_lexic(lexic::errors);
            return;
        }
        queue<lexic::LexicNode> input_queue = process_lexic_results(lexic::results_lexic);
        input_queue.push(lexic::LexicNode("$",0,""));

        stack<string> productions_stack;

        productions_stack.push("$");
        productions_stack.push("S");

        while (productions_stack.top() != "$" && input_queue.size() > 0) {

//            cout << endl;
//            cout << productions_stack.top() << endl;
//            cout <<  input_queue.front().token << endl;
//            cout << input_queue.front().n_line << endl;
//            cout << endl;
            if( productions_stack.top() == "€" ){
                productions_stack.pop();
                continue;
            }

            if( is_terminal(productions_stack.top()) && productions_stack.top() == input_queue.front().token ){
                input_queue.pop();
                productions_stack.pop();
                continue;
            }


            list<string> item = grammar_table::get_item_table(productions_stack.top(), input_queue.front().token);

            if(item.size() < 1){
                cout << endl;
                cout << productions_stack.top() << endl;
                cout <<  input_queue.front().token << endl;
                cout << endl;
                cout << "Syntax Error at line  " << input_queue.front().n_line << "  at lex  " << input_queue.front().lex << endl;
                if(is_terminal(productions_stack.top())){
                    cout << productions_stack.top() << " Expected instead of" << input_queue.front().token << endl;
                }
                input_queue.pop();
                continue;
            }
            productions_stack.pop();
            for(auto it = item.rbegin(); it != item.rend(); ++it){
                productions_stack.push((*it));
            }

        }
    }

}



#endif // SYNTAXIC_H
